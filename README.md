# README #

# Development Process

## Creating a bugfix
- **Pull** `develop`
- Create branch on local with pattern `bugfix/[TicketNo.] | [issueTitle]`
- Write amazing code
- Test on development store
- **Push** to branch on repo
- Create **pull** **request** to merge onto ``develop`` (to be reviewed by a tech lead or senior developer)

## Creating a hotfix 
- **Pull** `master`
- Create branch on local with pattern `hotfix/[TicketNo.] | [issueTitle]`
- Write amazing code
- Test on development store
- **Push** to branch on repm
- Create **pull request** to merge onto `master` (to be reviewed by a tech lead or senior developer)
- Create **pull request** to merge onto ``develop`` (to be reviewed by a tech lead or senior developer)


# Deployment Process

- Create a `release/` branch based of the latest version of development 
- Create a **pull request** to merge the new `release` into `master` (to be reviewed by a tech lead or senior developer)
- Once approved **pull** latest version of `master` onto local repository